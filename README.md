# **ionic**
based on main tutorial at [Thinkster](https://thinkster.io/ionic-framework-tutorial)
______

    Ionic is a powerful HTML5 SDK that helps you build native-feeling mobile apps using web technologies like HTML, CSS, and Javascript.

    Ionic is a great solution for creating both mobile web apps and native apps

### Read
  + [PhoneGap](http://phonegap.com/)
    + PhoneGap is a free and open source framework that allows you to create mobile apps using standardized web APIs for iOS n Android
  + [Cordova](http://cordova.apache.org/)
      + Target multiple platforms with one code base Free and open source
  + [Trigger.io](https://trigger.io/)
    + Build native apps for iOS and Android using JavaScript
+ The difference between Cordova and PhoneGap? Adobe uses a helpful analogy: Cordova is to PhoneGap as [Blink](https://en.wikipedia.org/wiki/Blink_%28web_engine%29) is to Chrome. PhoneGap is Cordova plus extra Adobe stuff.


  + [Where to see ionic](http://blog.ionic.io/where-does-the-ionic-framework-fit-in/)
  + [Overview of ionic](http://ionicframework.com/docs/overview/)


    So, when you start a new hybrid app project, you can either decide to use Cordova proper, or enter into Adobe’s ecosystem and use the PhoneGap distribution of Cordova.

    ~Note: Ionic uses Cordova proper at the core, we do not use PhoneGap at all.

#### Tutorial Flow
1. Structuring Ionic apps
1. Packaging that same exact code into a native app, using Cordova
____

## **Getting Started**

+ Install Node.js
       curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
+ Install ionic
      npm install -g ionic
+ Clone this repo

+ from code folder run
      ionic serve
      npm install before the other
